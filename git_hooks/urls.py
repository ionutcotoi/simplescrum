__author__ = 'cotty'
from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # frontend urls
    url(r'^notifyCommit/$', 'git_hooks.views.notify_commit', name='notify_commit'),
)
