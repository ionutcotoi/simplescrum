#!/usr/bin/env python
import os
import sys
import csv

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "simplescrum.settings")

    from django.core.management import execute_from_command_line
    import django
    from issues.models import Issue, Project
    from django.contrib.auth.models import User

    django.setup()

    filename = sys.argv[2]
    try:
        with open(filename, 'rb') as csvfile:
            taskreader = csv.reader(csvfile, delimiter=',')
            for row in list(taskreader)[1:]:
                title = row[1]
                responsible = row[2]
                description = row[3]
                deadline = row[4]
                user = User.objects.get(username=responsible)
                project = sys.argv[1]
                this_project = Project.objects.get(name=project)
                iss = Issue()
                iss.project = this_project
                iss.assignee = user
                iss.creator = User.objects.get(username="ionut.cotoi")
                iss.name = title
                iss.description = description
                iss.group = this_project.group
                iss.save()
                print iss
    except Exception as e:
        print e