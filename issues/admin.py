from django.contrib import admin
from models import Issue, Project, ActionLog, UserDetail, Sprint

admin.site.register(Issue)
admin.site.register(Project)
admin.site.register(UserDetail)
admin.site.register(ActionLog)
admin.site.register(Sprint)