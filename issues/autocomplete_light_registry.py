__author__ = 'cotty'
from models import Issue, Project
import autocomplete_light
from taggit.models import Tag


autocomplete_light.register(Issue)
autocomplete_light.register(Project)
autocomplete_light.register(Tag)