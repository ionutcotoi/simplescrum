from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from models import Project, Issue, ISSUE_PRIORITY_CHOICES, ISSUE_STATUS_CHOICES, UserDetail
from docutils.core import publish_parts
import json
from post_office import mail



@login_required
def issue(request, org_id, projectId, issueId):
    # project = get_object_or_404(pk=projectId)
    issue = get_object_or_404(Issue, pk=issueId)

    user_groups = request.user.groups.values_list('id')

    selectedProjectName = "All Projects"

    if projectId != None:
        if int(projectId) == 0:
            selectedProjectName = "Bulk items"
        else:
            sp = Project.objects.get(pk=int(projectId))
            selectedProjectName = sp.name


    return render_to_response('issue.html', RequestContext(request, {
        'users': User.objects.filter(groups__in=user_groups).order_by("username"),
        'prios': ISSUE_PRIORITY_CHOICES,
        'statuses': ISSUE_STATUS_CHOICES,
        'projects': Project.objects.filter(group__in=user_groups).order_by("name"),
        'projectId': projectId,
        'projectName': selectedProjectName,
        'issue': issue,
        'selected_org': org_id,
    }))