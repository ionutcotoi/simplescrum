from django.core.management.base import BaseCommand, CommandError
from issues.models import UserDetail
from django.contrib.auth.models import User
from django_gravatar.helpers import get_gravatar_url, has_gravatar, get_gravatar_profile_url, calculate_gravatar_hash


class Command(BaseCommand):
    help = 'Update gravatar information for registered users in the system'

    def handle(self, *args, **options):
        for user in User.objects.all():
            try:
                user_detail = UserDetail.objects.get(user=user)
            except Exception as e:
                user_detail = UserDetail()
                user_detail.user = user
                user_detail.save()
        for user_det in UserDetail.objects.all():
            if has_gravatar(user_det.user.email):
                user_det.gravatar_img_url = get_gravatar_url(user_det.user.email, size=24)
            else:
                user_det.gravatar_img_url = "http://dummyimage.com/24x24/fff/000.png&text=" + str(user_det.user.username[0:2])
            user_det.save()

        print "Updated gravatars!"
