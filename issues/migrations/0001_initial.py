# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ActionLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField(auto_now_add=True)),
                ('action', models.CharField(default=b'', max_length=128, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Issue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=64)),
                ('description', models.TextField(default=b'', max_length=250, blank=True)),
                ('priority', models.CharField(default=b'2', max_length=1, choices=[(b'0', b'HIGH'), (b'1', b'MEDIUM'), (b'2', b'LOW')])),
                ('status', models.CharField(default=b'0', max_length=1, choices=[(b'0', (b'todo', b'TO DO')), (b'1', (b'inprogress', b'IN PROGRESS')), (b'2', (b'done', b'DONE'))])),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('modification_date', models.DateTimeField(auto_now_add=True)),
                ('estimate', models.IntegerField(null=True)),
                ('assignee', models.ForeignKey(related_name='assignee', to=settings.AUTH_USER_MODEL, null=True)),
                ('creator', models.ForeignKey(related_name='creator', to=settings.AUTH_USER_MODEL)),
                ('group', models.ForeignKey(to='auth.Group', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=64)),
                ('description', models.TextField(default=b'', max_length=250, blank=True)),
                ('creator', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('group', models.ForeignKey(to='auth.Group', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sprint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=32, null=True, blank=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('completion_date', models.DateTimeField()),
                ('status', models.CharField(default=b'0', max_length=1, choices=[(b'0', (b'todo', b'TO DO')), (b'1', (b'inprogress', b'IN PROGRESS')), (b'2', (b'done', b'DONE'))])),
                ('creator', models.ForeignKey(related_name='sprint_creator', to=settings.AUTH_USER_MODEL)),
                ('owner', models.ForeignKey(related_name='sprint_owner', to=settings.AUTH_USER_MODEL, null=True)),
                ('project', models.ForeignKey(to='issues.Project')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gravatar_img_url', models.CharField(default=b'', max_length=256, blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='issue',
            name='project',
            field=models.ForeignKey(to='issues.Project', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='actionlog',
            name='issue',
            field=models.ForeignKey(to='issues.Issue'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='actionlog',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
