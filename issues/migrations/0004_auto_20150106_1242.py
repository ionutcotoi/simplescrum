# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('issues', '0003_issue_tags'),
    ]

    operations = [
        migrations.RenameField(
            model_name='issue',
            old_name='title',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='project',
            old_name='title',
            new_name='name',
        ),
    ]
