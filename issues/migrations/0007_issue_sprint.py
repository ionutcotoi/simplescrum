# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('issues', '0006_issue_notify_users'),
    ]

    operations = [
        migrations.AddField(
            model_name='issue',
            name='sprint',
            field=models.ForeignKey(to='issues.Sprint', null=True),
            preserve_default=True,
        ),
    ]
