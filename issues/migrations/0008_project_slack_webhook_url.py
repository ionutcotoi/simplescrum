# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('issues', '0007_issue_sprint'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='slack_webhook_url',
            field=models.CharField(default=b'', max_length=250, blank=True),
            preserve_default=True,
        ),
    ]
