# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('issues', '0008_project_slack_webhook_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='slack_channel',
            field=models.CharField(default=b'#devicehub', max_length=64, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='project',
            name='slack_notification',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
