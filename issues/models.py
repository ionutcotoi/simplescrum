from django.db import models
from django.contrib.auth.models import User, Group
from taggit.managers import TaggableManager

ISSUE_PRIORITY_CHOICES = (
    ('0', 'HIGH'),
    ('1', 'MEDIUM'),
    ('2', 'LOW'),
)

ISSUE_STATUS_CHOICES = (
    ('0', ('todo', "TO DO")),
    ('1', ('inprogress', "IN PROGRESS")),
    ('2', ('done', "DONE")),
)


class UserDetail(models.Model):
    user = models.OneToOneField(User)
    gravatar_img_url = models.CharField(max_length=256, default="", blank=True)

    def __unicode__(self):
        return u"%s" % (self.user, )


class Project(models.Model):
    creator  = models.ForeignKey(User)
    name    = models.CharField(max_length=64)
    description = models.TextField(max_length=250, default="", blank=True)
    group = models.ForeignKey(Group, null=True)
    slack_notification = models.BooleanField(default=False)
    slack_webhook_url = models.CharField(max_length=250, default="", blank=True)
    slack_channel = models.CharField(max_length=64, default="#devicehub", blank=True)

    def __unicode__(self):
        return u"%s (%s)" % (self.name, self.creator)


class Sprint(models.Model):
    project = models.ForeignKey(Project)
    creator = models.ForeignKey(User, related_name="sprint_creator")
    owner = models.ForeignKey(User, related_name="sprint_owner", null=True)
    title = models.CharField(max_length=32, null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    completion_date = models.DateTimeField()
    status = models.CharField(max_length=1, choices=ISSUE_STATUS_CHOICES, default='0')

    def __unicode__(self):
        return u'%s - %d' % (self.project.name, self.pk)
    

class Issue(models.Model):
    creator  = models.ForeignKey(User, related_name="creator")
    assignee = models.ForeignKey(User, related_name="assignee", null=True)
    name    = models.CharField(max_length=128)
    description = models.TextField(max_length=250, default="", blank=True)
    project  = models.ForeignKey(Project, null=True)
    priority = models.CharField(max_length=1, choices=ISSUE_PRIORITY_CHOICES, default='2')
    status = models.CharField(max_length=1, choices=ISSUE_STATUS_CHOICES, default='0')
    creation_date = models.DateTimeField(auto_now_add=True)
    modification_date = models.DateTimeField(auto_now_add=True)
    estimate = models.IntegerField(null=True)
    group = models.ForeignKey(Group, null=True)
    commit_hash = models.CharField(max_length=128, default="", blank=True)
    notify_users = models.ManyToManyField(User)
    sprint = models.ForeignKey(Sprint, null=True)
    tags = TaggableManager()

    def __unicode__(self):
        return u'#%d - %s (%s, %s)' % (self.id, self.name, self.creator, self.assignee)


class ActionLog(models.Model):
    user = models.ForeignKey(User)
    issue = models.ForeignKey(Issue)
    time = models.DateTimeField(auto_now_add=True)
    action = models.CharField(max_length=128, default="", blank=True)

    def __unicode__(self):
        return u"%s > %d - %s: %s" % (self.time, self.issue.id, self.issue.name, self.action)


