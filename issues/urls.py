__author__ = 'cotty'
from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # frontend urls
    url(r'^$', 'issues.views.home', name='home'),
    url(r'^org/(?P<org_id>\d+)/$', 'issues.views.home', name='home'),
    url(r'^project/$', 'issues.views.project', name='project'),
    url(r'^org/(?P<org_id>\d+)/project/$', 'issues.views.project', name='project'),
    url(r'^org/(?P<org_id>\d+)/project/(?P<projectId>\d+)/$', 'issues.views.home', name='home'),
    url(r'^org/(?P<org_id>\d+)/project/(?P<projectId>\d+)/issue/(?P<issueId>\d+)$', 'issues.frontend_views.issue', name='frontend_issue'),
    url(r'^project/(?P<projectId>\d+)/$', 'issues.views.home', name='home'),
    url(r'^project/(?P<projectId>\d+)/issue/(?P<issueId>\d+)$', 'issues.frontend_views.issue', name='frontend_issue'),
    # API urls
    url(r'^issue/?(?:/(?P<issueId>\d+))?/$', 'issues.views.issue', name='issue'),
    url(r'^issues/$', 'issues.views.issues', name='issues'),
    url(r'^org/(?P<org_id>\d+)/issues/$', 'issues.views.issues', name='issues'),
)
