from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render, get_object_or_404
from django.template.context import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from models import Project, Issue, ISSUE_PRIORITY_CHOICES, ISSUE_STATUS_CHOICES, UserDetail
from docutils.core import publish_parts
import json
from post_office import mail
from django.contrib.comments.models import Comment
from django.contrib.comments.signals import comment_was_posted
from django.dispatch import receiver
from django.core.exceptions import ValidationError
import urllib


def send_slack_notification(channel, user, subject, message, ticket_url, hook_url, ticket=None, comment=None):

    if ticket.assignee is not None:
        username = ticket.assignee.username
    else:
        username = "UNASSIGNED!"
    try:
        data = {
            "channel": channel,
            "username": user.username,
            "text": subject,
            "attachments": [
                {
                    "fallback": "",
                    "color": "good",
                    "fields": [
                        {
                            "title": "Link",
                            "value": "<http://"+ticket_url+">",
                            "short": False,
                        },
                        {
                            "title": "Status",
                            "value": ISSUE_STATUS_CHOICES[int(ticket.status)][1],
                            "short": True,
                        },
                        {
                            "title": "Priority",
                            "value": ISSUE_PRIORITY_CHOICES[int(ticket.priority)][1],
                            "short": True,
                        },
                        {
                            "title": "Owner",
                            "value": username,
                            "short": True,
                        }
                    ]
                }
            ]
        }

        if comment is not None:
            data['attachments'][0]['fields'].append({
                "title": "comment",
                "value": comment,
                "short": False
            })

        data = "payload=" + str(json.dumps(data))
        print "json:", data
        print "data:", data
        f = urllib.urlopen(hook_url, data)
        print "slack resp:", f.read()
    except Exception as e:
        print e


def notify_users(request, subject, html_message, iss=None):
    """
    Used to notify users of comments
    :param request:
    :param subject:
    :param html_message:
    :param iss:
    :return:
    """
    to_list = []

    if iss is not None:
        if request.user.email != iss.creator.email:
            to_list.append(iss.creator.email)

        if iss.assignee is not None and request.user.email != iss.assignee.email:
            to_list.append(iss.assignee.email)

    if 'object_pk' in request.POST:
        object_pk = request.POST.get('object_pk')
        comments = Comment.objects.filter(object_pk=str(object_pk))
        for comment in comments:
            if comment.user.email not in to_list and comment.user.email != request.user.email:
                to_list.append(comment.user.email)

    slack_message = html_message

    # Add the issue link to the message body
    ticket_url = request.get_host() + "/org/" + str(iss.group_id) + "/project/" + str(iss.project_id) + "/issue/" + str(iss.id)
    html_message += "<HR>" + ticket_url


    # Send the mail
    mail.send(to_list,
              sender=request.user.email,
              subject=subject,
              html_message=html_message)

    if iss.project and iss.project.slack_notification:
        send_slack_notification(iss.project.slack_channel,
                                request.user,
                                subject,
                                slack_message,
                                ticket_url,
                                iss.project.slack_webhook_url,
                                ticket=iss,
                                comment=slack_message)


@receiver(comment_was_posted, sender=Comment)
def comment_post_callback(request, sender, **kwargs):
    object_pk = request.POST.get('object_pk')
    iss = Issue.objects.get(pk=object_pk)

    # TODO Investigate templates https://github.com/ui/django-post_office
    subject = "Tasks: " + request.user.username + " commented on task #"+str(iss.id)+" - "+iss.name
    message_body = str(request.POST.get('comment'))
    notify_users(request, subject, message_body, iss)


def issues_to_json(issues):
    output = [{
                  "id": x['id'],
                  "name": x['name'],
                  "description": publish_parts(x['description'], writer_name='html')['html_body'],
                  "description_raw": x['description'],
                  "priority": int(x['priority']),
                  "priority_str": ISSUE_PRIORITY_CHOICES[int(x['priority'])][1],
                  "status": int(x['status']),
                  "status_str": ISSUE_STATUS_CHOICES[int(x['status'])][1][0],
                  "creator": x['creator__username'],
                  "assignee": x['assignee__username'],
                  "assignee_id": x['assignee__id'],
                  "assignee_icon": "",
                  "assignee_has_icon": False,
                  "assignee_name": x['assignee__username'],
                  "project": x['project__name'],
                  "project_id": x['project__id'] or 0,
                  "estimate": x['estimate'],
                  "org_id": x['project__group__id'],
                  "comments_no": int(Comment.objects.filter(object_pk=x['id']).count()),
              } for x in issues]

    for x in output:
        if x['assignee_id'] is not None:
            x['assignee_icon'] = UserDetail.objects.filter(user__id=x['assignee_id']).values("gravatar_img_url")[0]['gravatar_img_url']
            if x['assignee_icon'] != "":
                x['assignee_has_icon'] = True
            assignee_full_name = User.objects.filter(pk=x['assignee_id']).values("first_name", "last_name")[0]
            if assignee_full_name['first_name'] != "" and assignee_full_name['last_name'] != "":
                x['assignee_name'] = assignee_full_name['first_name'] + " " + assignee_full_name['last_name']
            else:
                x['assignee_name'] = x['assignee'][0] + " " + x['assignee'][1]
        else:
            x['assignee_name'] = "N A"

    output = json.dumps(output, indent=2)
    return output


@login_required
def home(request, org_id=None, projectId=None):

    if org_id is None:
        org_id = request.user.groups.values_list('id')[0][0]
        return HttpResponseRedirect("/org/" + str(org_id))

    selectedProjectName = "All Projects"

    if org_id is None:
        user_groups = request.user.groups.values_list('id')
        users = User.objects.filter(groups__in=user_groups).order_by("username")
        projects = Project.objects.filter(group__in=user_groups).order_by("name")
    else:
        users = User.objects.filter(groups__in=(str(org_id), )).order_by("username")
        projects = Project.objects.filter(group_id=str(org_id)).order_by("name")

    if projectId != None:
        if int(projectId) == 0:
            selectedProjectName = "Bulk items"
        else:
            sp = Project.objects.get(pk=int(projectId))
            selectedProjectName = sp.name

    return render_to_response('home.html', RequestContext(request, {
        'users': users,
        'prios': ISSUE_PRIORITY_CHOICES,
        'statuses': ISSUE_STATUS_CHOICES,
        'projects': projects,
        'projectId': projectId,
        'projectName': selectedProjectName,
        'selected_org': org_id,
    }))


@csrf_exempt
def issue(request, issueId):
    if request.method == "DELETE":
        data = {
            "success": True,
            "msg": "Issue deleted",
        }

        iss = Issue.objects.get(pk=issueId)
        if iss.creator == request.user:
            # Send email if assignee != creator
            if iss.assignee is not None and iss.assignee != iss.creator:
                subject = "Task #"+str(iss.id)+" - "+iss.name+" - has been deleted"
                notify_users(request, subject, iss.description, iss)
            iss.delete()
            output = json.dumps(data)
            HttpResponse(output, content_type="application/json", status=200)
        else:
            data['success'] = False
            data['error_msg'] = "This issue was not created by you!!!"

            output = json.dumps(data)
            HttpResponse(output, content_type="application/json", status=403)
    elif request.method == "GET":
        if issueId is not None:
            issue = Issue.objects.filter(pk=issueId).values("id", "name", "description", "priority", "status", "estimate",
                                                            "creator__username",
                                                            "assignee__username", "assignee__email", "assignee__id",
                                                            "project__name", "project__id", "project__group__id")
            output = issues_to_json(issue)
        else:
            data = {
                "success": False,
                "error_msg": "invalid issue id!",
            }
            output = json.dumps(data)
    elif request.method == "POST":
        if issueId is None:
            # TODO add exception handling
            data = json.loads(request.body)
            print data
            newIssue = Issue()
            newIssue.name = data['name']
            newIssue.description = data['description']
            newIssue.status = data['status']
            newIssue.creator = request.user
            if data['project_id'] != "None":
                proj = Project.objects.get(pk=int(data['project_id']))
                newIssue.project = proj
            if data['assignee'] != 0:
                ass = User.objects.get(pk=data['assignee'])
                newIssue.assignee = ass
            if "estimate" in data:
                newIssue.estimate = data['estimate']
            newIssue.priority = str(data['priority'])

            user_groups = request.user.groups.values_list('id')

            # print user_groups[0][0]

            if len(user_groups) == 1:
                newIssue.group_id = user_groups[0][0]
            else:
                newIssue.group_id = 1

            newIssue.save()

            # Send email if assignee != creator
            if newIssue.assignee is not None and newIssue.assignee != newIssue.creator:
                try:
                    subject = "Task #"+str(newIssue.id)+" - "+newIssue.name+" - has been assigned to you",
                    notify_users(request, subject, newIssue.description, newIssue)
                except ValidationError:
                    data = {
                        "success": False,
                        "error_msg": "Cannot send email. Check that you have configured your email address in profile!",
                    }
                    # return HttpResponse(json.dumps(data), status=403, content_type="application/json")

            if newIssue.project is not None and newIssue.project.slack_notification:
                send_slack_notification(newIssue.project.slack_channel,
                                        newIssue.creator,
                                        "New task created: #" + str(newIssue.id) + " - " + newIssue.name,
                                        newIssue.description,
                                        request.get_host() + "/org/" + str(newIssue.group_id) + "/project/" + str(newIssue.project_id) + "/issue/" + str(newIssue.id),
                                        newIssue.project.slack_webhook_url,
                                        ticket=newIssue)

            data = {
                "success": True,
                "msg": "New issue #" + str(newIssue.id) + " added!",
            }
            output = json.dumps(data)
        else:
            # print request.body
            data = json.loads(request.body)
            print "Issue update:", data
            try:
                issue = Issue.objects.get(pk=issueId)
            except:
                data = {
                    "success": False,
                    "error_msg": "invalid issue id!",
                }
                output = json.dumps(data)
            else:
                issue.status = data['status']
                if 'project_id' in data and data['project_id'] != 'None' and data['project_id'] is not None:
                    print data['project_id']
                    proj = Project.objects.get(pk=data['project_id'])
                    issue.project = proj
                if 'description' in data:
                    issue.description = data['description']
                if 'assignee' in data and data['assignee'] is not None and data['assignee'] != 0:
                    ass = User.objects.get(pk=data['assignee'])
                    issue.assignee = ass
                if 'name' in data:
                    issue.name = data['name']
                if 'priority' in data:
                    issue.priority = str(data['priority'])
                if 'estimate' in data:
                    issue.estimate = data['estimate']
                issue.save()
                try:
                    subject = "Task #"+str(issue.id)+" - "+issue.name+" - has been updated"
                    notify_users(request, subject, issue.description, issue)
                except Exception as e:
                    pass
                data = {
                    "success": True,
                    "msg": "item updated",
                }
                output = json.dumps(data)
    else:
        data = {
            "success": False,
            "error_msg": "invalid request!",
        }
        output = json.dumps(data)
    return HttpResponse(output, content_type="application/json")


def issues(request, org_id=None):
    iss = Issue.objects.values("id", "name", "description", "priority", "status",
                                                                      "estimate", "creator__username", "assignee__username",
                                                                      "assignee__email", "assignee__id", "project__name",
                                                                      "project__id", "project__group__id").order_by('priority')
    if org_id is None:
        user_groups = request.user.groups.values_list('id')
        iss = iss.filter(group__in=user_groups)
    else:
        iss = iss.filter(group=int(org_id))

    if request.GET.get('projectId') == "0":
        iss = iss.filter(project__id=None)
    elif request.GET.get('projectId') is not None and request.GET.get('projectId') != "None":
        iss = iss.filter(project__id=request.GET.get('projectId'))

    output = issues_to_json(iss)
    return HttpResponse(output, content_type="application/json")


@csrf_exempt
def project(request, org_id=None):
    if request.method == "POST":
        data = json.loads(request.body)
        new_proj = Project()
        new_proj.creator_id = data['creator']
        new_proj.name = data['name']
        new_proj.description = data['description']
        if org_id is not None:
            new_proj.group_id = org_id
            new_proj.save()
            data = {
                "success": True,
                "msg": "Project added"
            }
            return HttpResponse(json.dumps(data), content_type="application/json", status=200)
        elif "org_id" in data:
            new_proj.group_id = data["org_id"]
            new_proj.save()
            data = {
                "success": True,
                "msg": "Project added"
            }
            return HttpResponse(json.dumps(data), content_type="application/json", status=200)
        else:
            data = {
                "success": False,
                "error_msg": "Missing organization from request data!"
            }
            return HttpResponse(json.dumps(data), content_type="application/json", status=403)
    else:
        data = {
            "success": False,
            "error_msg": "Invalid request!"
        }
        return HttpResponse(json.dumps(data), content_type="application/json", status=403)