from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('issues.urls')),
    url(r'^git/', include('git_hooks.urls')),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    url(r'^wiki/', include('waliki.urls')),
    url(r'^comments/', include('django.contrib.comments.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'template_name': 'logout.html'}),
    url(r'^accounts/password_change/$', 'django.contrib.auth.views.password_change', {'template_name': 'changepasswd.html'}),
    url(r'^accounts/password_change_done/$', 'django.contrib.auth.views.password_change_done', {'template_name': 'changepasswd_done.html'}, name='password_change_done'),
    url(r'accounts/profile/$', RedirectView.as_view(url='/'), name='profile'),
    url(r'^admin/', include(admin.site.urls)),
    # django-rq
    # url(r'^django-rq/', include('django_rq.urls')),
)
